package com.example.lesson4

class Area(private val name: String, private val cityList: List<City>) : GetName {
    override fun getName(): String {
        return name
    }

    fun getCityList(): List<City> {
        return cityList
    }
}