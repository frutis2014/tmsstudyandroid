package com.example.lesson4

class Country(
    private val name: String,
    private val square: Int,
    private val regionList: List<Region>
) {
    fun findCapital(): String? {
        if (checkNumOfCapital() == 1)
            for (region in regionList)
                for (area in region.getAreaList())
                    for (city in area.getCityList())
                        return if (city.isCapital())
                            "${city.getName()} is capital of $name"
                        else null
        else return "Country must have capital(or have only 1 capital)!"
        return null
    }

    private fun checkNumOfCapital(): Int {
        var count = 0
        for (region in regionList)
            for (area in region.getAreaList())
                for (city in area.getCityList())
                    if (city.isCapital())
                        count++
        return count
    }

    fun findRegionNum(): String {
        return "${regionList.size} is number of region in $name"
    }

    fun getSquare(): String {
        return "$square km^2 is square of $name"
    }

    fun findRegionCenters(): String {
        val listOfCenters = mutableListOf<String>()
        if (chekNumOfRegionCenter() != (regionList.size + 1))
            for (region in regionList)
                for (area in region.getAreaList())
                    for (city in area.getCityList())
                        if (city.isRegionCenter())
                            listOfCenters.add(city.getName())
        return if (listOfCenters.size == 1)
            "$listOfCenters this is region center of $name"
        else "$listOfCenters these are region centers of $name "
    }

    private fun chekNumOfRegionCenter(): Int {
        var count = 0
        for (region in regionList)
            for (area in region.getAreaList())
                for (city in area.getCityList())
                    if (city.isRegionCenter())
                        count++
        return count
    }
}