package com.example.lesson4

class Region(private val name: String, private val areaList: List<Area>) : GetName {
    override fun getName(): String {
        return name
    }

    fun getAreaList(): List<Area> {
        return areaList
    }

}