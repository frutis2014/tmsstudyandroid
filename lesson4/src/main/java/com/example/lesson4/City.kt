package com.example.lesson4

class City(
    private val name: String,
    private val isCapital: Boolean,
    private val isRegionCenter: Boolean
) : GetName {
    override fun getName(): String {
        return name
    }

    fun isCapital(): Boolean {
        return isCapital
    }

    fun isRegionCenter(): Boolean {
        return isRegionCenter
    }

}

