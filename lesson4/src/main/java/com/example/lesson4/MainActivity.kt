package com.example.lesson4

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val minsk = City("Minsk", isCapital = true, isRegionCenter = true)
        val brest = City("Brest", isCapital = false, isRegionCenter = true)
        val dzerzhinsk = City("Dzerzhinsk", isCapital = false, isRegionCenter = false)
        val baranovichi = City("Baranovichi", isCapital = false, isRegionCenter = false)
        val cityListOfMinskArea = listOf(minsk, dzerzhinsk)
        val cityListOfBrestArea = listOf(brest, baranovichi)
        val minskArea = Area("Minsk area", cityListOfMinskArea)
        val brestArea = Area("Brest area", cityListOfBrestArea)
        val areaListOfMinskRegion = listOf(minskArea)
        val areaListOfBrestRegion = listOf(brestArea)
        val minskRegion = Region("Minsk region", areaListOfMinskRegion)
        val brestRegion = Region("Brest region", areaListOfBrestRegion)
        val regionListOfBelarus = listOf(minskRegion, brestRegion)
        val country = Country("Belarus", 13000, regionListOfBelarus)
        val showCapital: TextView = findViewById(R.id.showCapital)
        val showSquare: TextView = findViewById(R.id.showSquare)
        val showRegionNumber: TextView = findViewById(R.id.showRegionNumber)
        val showRegionCenters: TextView = findViewById(R.id.showRegionCenters)
        showCapital.text = country.findCapital()
        showRegionNumber.text = country.findRegionNum()
        showRegionCenters.text = country.findRegionCenters()
        showSquare.text = country.getSquare()
    }

}