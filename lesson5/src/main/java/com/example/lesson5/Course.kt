package com.example.lesson5

class Course(private val name: String, private val groupList: List<Group>) {
    fun getName(): String {
        return name
    }

    fun getGroupList(): List<Group> {
        return groupList
    }
}