package com.example.lesson5

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val findOldestStudent = findViewById<Button>(R.id.findOldest)
        val findYoungestStudent = findViewById<Button>(R.id.findYoungest)
        val averageMark = findViewById<Button>(R.id.avMark)
        val showStudent = findViewById<Button>(R.id.showStudent)
        val averageAge = findViewById<Button>(R.id.showAverageAge)
        val input = Intent(this@MainActivity, InputActivity::class.java)
        val show = Intent(this@MainActivity, ShowInformation::class.java)
        val choice = Intent(this@MainActivity, ChooseOption::class.java)
        findOldestStudent.setOnClickListener{
            show.putExtra("SHOW",DataBase.getRTI().getOldestStudent())
            startActivity(show)
        }
        findYoungestStudent.setOnClickListener{
            show.putExtra("SHOW",DataBase.getRTI().getYoungestStudent())
            startActivity(show)
        }
        showStudent.setOnClickListener{
            startActivity(choice)
        }
        averageMark.setOnClickListener{
            input.putExtra("CASE",Choice.AVERAGE)
            startActivity(input)
        }
        averageAge.setOnClickListener{
            show.putExtra("SHOW",DataBase.getRTI().getAverageAge())
            startActivity(show)
        }
    }
}