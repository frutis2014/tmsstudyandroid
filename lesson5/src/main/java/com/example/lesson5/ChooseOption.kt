package com.example.lesson5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ChooseOption : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_option)
        val showByGroup = findViewById<Button>(R.id.showByGroup)
        val showByCourse = findViewById<Button>(R.id.showByCourse)
        val showByUniversity = findViewById<Button>(R.id.showByUniversity)
        val input = Intent(this@ChooseOption, InputActivity::class.java)
        val show = Intent(this@ChooseOption, ShowInformation::class.java)
        showByGroup.setOnClickListener() {
            input.putExtra("CASE",Choice.GROUP)
            startActivity(input)
        }
        showByCourse. setOnClickListener (){
            input.putExtra("CASE",Choice.COURSE)
            startActivity(input)
        }
        showByUniversity. setOnClickListener (){
            show.putExtra("SHOW",DataBase.getRTI().getStudent(Choice.UNIVERSITY,";"))
            startActivity(show)
        }
    }
}