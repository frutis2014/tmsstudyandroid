package com.example.lesson5

class Student(
    private val name: String,
    private val secondName: String,
    private val age: Double,
    private val averageMark: Double
) {
    fun getName(): String {
        return name
    }

    fun getSecondName(): String {
        return secondName
    }

    fun getAge(): Double {
        return age
    }

    fun getAverageMark(): Double {
        return averageMark
    }
}