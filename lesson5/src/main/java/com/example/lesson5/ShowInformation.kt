package com.example.lesson5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShowInformation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_information)
        val showInfo = findViewById<TextView>(R.id.showInfo)
        showInfo.text = intent.extras?.getString("SHOW")

    }
}