package com.example.lesson5

class Group(private val name: String, private val studentList: List<Student>) {
    fun getName(): String {
        return name
    }

    fun getStudentList(): List<Student> {
        return studentList
    }
}