package com.example.lesson5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class InputActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_activity)
        val input = findViewById<Button>(R.id.button)
        val editText = findViewById<EditText>(R.id.input)
        val show = Intent(this@InputActivity, ShowInformation::class.java)
        val choice: Choice = intent.extras?.get("CASE") as Choice
        input.setOnClickListener {
            when (choice) {
                Choice.AVERAGE -> {
                    show.putExtra(
                        "SHOW",
                        DataBase.getRTI()
                            .getNumOfStudentWithAvMarkMore(editText.text.toString().toDouble())
                    )
                    startActivity(show)
                }
                else -> {
                    show.putExtra(
                        "SHOW",
                        DataBase.getRTI().getStudent(choice, editText.text.toString())
                    )
                    startActivity(show)
                }
            }

        }

    }

}