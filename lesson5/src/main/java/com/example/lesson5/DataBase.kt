package com.example.lesson5

object DataBase {
    private val student1 = Student("Pasha", "Ovcev", 18.0, 9.0)
    private val student2 = Student("Vova", "Voronov", 21.0, 6.5)
    private val student3 = Student("Alex", "Brigantinov", 23.0, 4.5)
    private val student4 = Student("Andrey", "Oshmianov", 24.6, 7.5)
    private val student5 = Student("Petya", "Peskov", 25.0, 10.0)
    private val student6 = Student("Victor", "Dimitrov", 20.0, 4.9)
    private val student7 = Student("Artiom", "Sidorov", 21.5, 7.8)
    private val student8 = Student("Nikita", "Panasiuk", 17.0, 9.5)
    private val studentListOfGroup1 = listOf<Student>(student1, student2)
    private val studentListOfGroup2 = listOf<Student>(student3, student4)
    private val studentListOfGroup3 = listOf<Student>(student5, student6)
    private val studentListOfGroup4 = listOf<Student>(student7, student8)
    private val group1 = Group("g110", studentListOfGroup1)
    private val group2 = Group("g111", studentListOfGroup2)
    private val group3 = Group("g112", studentListOfGroup3)
    private val group4 = Group("g113", studentListOfGroup4)
    private val groupListOfFRE = listOf<Group>(group1, group2)
    private val groupListOfKSIS = listOf<Group>(group3, group4)
    private val course1 = Course("FRE", groupListOfFRE)
    private val course2 = Course("KSIS", groupListOfKSIS)
    private val courseListOfRTI = listOf<Course>(course1, course2)
    private val rti = University("RTI", courseListOfRTI)
    fun getRTI():University{
        return rti
    }
}