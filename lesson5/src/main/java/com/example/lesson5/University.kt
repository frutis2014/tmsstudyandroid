package com.example.lesson5

class University(private val name: String, private val courseList: List<Course>) {

    fun getStudent(choice: Choice, name: String): String {
        when (choice) {
            Choice.COURSE -> {
                val mutableList = mutableListOf<String>()
                var count = 0
                for (course in courseList) {
                    if (name == course.getName()) {
                        for (group in course.getGroupList()) {
                            for (student in group.getStudentList()) {
                                mutableList.add(
                                    "${student.getName()} ${student.getSecondName()}," +
                                            " age is ${student.getAge()}, average mark is ${student.getAverageMark()}"
                                )
                                count++
                            }
                        }
                    }
                }
                return if (count > 0) {
                    mutableList.toString()
                } else {
                    "Input correct name of course"
                }
            }
            Choice.UNIVERSITY -> {
                val mutableList = mutableListOf<String>()
                for (course in courseList) {
                    for (group in course.getGroupList()) {
                        for (student in group.getStudentList()) {
                            mutableList.add(
                                "${student.getName()} ${student.getSecondName()}," +
                                        " age is ${student.getAge()}, average mark is ${student.getAverageMark()}"
                            )
                        }
                    }
                }
                return mutableList.toString()
            }

            Choice.GROUP -> {
                val mutableList = mutableListOf<String>()
                var count = 0
                for (course in courseList) {
                    for (group in course.getGroupList()) {
                        if (name == group.getName()) {
                            for (student in group.getStudentList()) {
                                mutableList.add(
                                    "${student.getName()} ${student.getSecondName()}," +
                                            " age is ${student.getAge()}, average mark is ${student.getAverageMark()}"
                                )
                                count++
                            }
                        }
                    }
                }
                return if (count > 0) {
                    mutableList.toString()
                } else {
                    "Input correct name of group"
                }
            }
            else -> {}
        }
        return " "
    }

    fun getAverageAge(): String {
        var ageTemp = 0.0
        var numOfStudent = 0.0
        for (course in courseList) {
            for (group in course.getGroupList()) {
                numOfStudent += group.getStudentList().size
                group.getStudentList().forEach{
                    ageTemp +=it.getAge()
                }
                    //for (student in group.getStudentList()) {
                    //ageTemp += student.getAge()
                //}
            }
        }
        val avAge = ageTemp / numOfStudent
        return "average age of student in $name is $avAge"
    }

    fun getOldestStudent(): String {
        var oldestStudent = Student("a", "b", 0.0, 0.0)
        for (course in courseList) {
            for (group in course.getGroupList()) {
                for (student in group.getStudentList()) {
                    if (student.getAge() > oldestStudent.getAge()) {
                        oldestStudent = student
                    }
                }
            }
        }
        return "Oldest student is ${oldestStudent.getName()} ${oldestStudent.getSecondName()}," +
                " age is ${oldestStudent.getAge()}"
    }

    fun getYoungestStudent(): String {
        var youngestStudent = Student("a", "b", 100.0, 200.0)
        for (course in courseList) {
            for (group in course.getGroupList()) {
                for (student in group.getStudentList()) {
                    if (student.getAge() < youngestStudent.getAge()) {
                        youngestStudent = student
                    }
                }
            }
        }
        return "Youngest student is ${youngestStudent.getName()} ${youngestStudent.getSecondName()}," +
                " age is ${youngestStudent.getAge()}"
    }

    fun getNumOfStudentWithAvMarkMore(mark: Double): String {
        val mutableList = mutableListOf<String>()
        var count = 0
        for (course in courseList) {
            for (group in course.getGroupList()) {
                for (student in group.getStudentList()) {
                    if (student.getAverageMark() > mark) {
                        mutableList.add(
                            "${student.getName()} ${student.getSecondName()}," +
                                    " average mark is ${student.getAverageMark()} "
                        )
                        count++
                    }
                }
            }
        }
        return if (count > 0) {
            mutableList.toString()
        } else {
            "No student with higher mark"
        }
    }
}