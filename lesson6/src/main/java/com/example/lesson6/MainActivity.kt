package com.example.lesson6

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import net.objecthunter.exp4j.ExpressionBuilder


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acrivity_main)
        var tempString = " "
        val oneButton = findViewById<Button>(R.id.oneButton)
        val twoButton = findViewById<Button>(R.id.twoButton)
        val threeButton = findViewById<Button>(R.id.threeButton)
        val fourButton = findViewById<Button>(R.id.fourButton)
        val fiveButton = findViewById<Button>(R.id.fiveButton)
        val sixButton = findViewById<Button>(R.id.sixButton)
        val sevenButton = findViewById<Button>(R.id.sevenButton)
        val eightButton = findViewById<Button>(R.id.eightButton)
        val nineButton = findViewById<Button>(R.id.nineButton)
        val zeroButton = findViewById<Button>(R.id.zeroButton)
        val plusButton = findViewById<Button>(R.id.plusButton)
        val minusButton = findViewById<Button>(R.id.minusButton)
        val multiplyButton = findViewById<Button>(R.id.multiplyButton)
        val divisionButton = findViewById<Button>(R.id.splitButton)
        val equalsButton = findViewById<Button>(R.id.equalsButton)
        val textView = findViewById<TextView>(R.id.textView)
        val clearButton = findViewById<Button>(R.id.clearButton)
        val deleteButton = findViewById<Button>(R.id.deleteButton)
        oneButton.setOnClickListener {
            tempString += "1"
            textView.text = tempString
        }
        twoButton.setOnClickListener {
            tempString += "2"
            textView.text = tempString
        }
        threeButton.setOnClickListener {
            tempString += "3"
            textView.text = tempString
        }
        fourButton.setOnClickListener {
            tempString += "4"
            textView.text = tempString
        }
        fiveButton.setOnClickListener {
            tempString += "5"
            textView.text = tempString
        }
        sixButton.setOnClickListener {
            tempString += "6"
            textView.text = tempString
        }
        sevenButton.setOnClickListener {
            tempString += "7"
            textView.text = tempString
        }
        eightButton.setOnClickListener {
            tempString += "8"
            textView.text = tempString
        }
        nineButton.setOnClickListener {
            tempString += "9"
            textView.text = tempString
        }
        zeroButton.setOnClickListener {
            tempString += "0"
            textView.text = tempString
        }
        plusButton.setOnClickListener {
            tempString += "+"
            textView.text = tempString
        }
        minusButton.setOnClickListener {
            tempString += "-"
            textView.text = tempString
        }
        divisionButton.setOnClickListener {
            tempString += "/"
            textView.text = tempString
        }
        multiplyButton.setOnClickListener {
            tempString += "*"
            textView.text = tempString
        }
        clearButton.setOnClickListener {
            tempString = ""
            textView.text = tempString
        }
        deleteButton.setOnClickListener {
            tempString = removeLastSymbol(tempString)
            textView.text = tempString
        }
        equalsButton.setOnClickListener {
            val text = textView.text.toString()
            val expression = ExpressionBuilder(text).build()
            try {
                val result = expression.evaluate()
                val longResult = result.toLong()
                if (result == longResult.toDouble()) {
                    textView.text = longResult.toString()
                } else {
                    textView.text = result.toString()
                }
            } catch (e: Exception) {
                Toast.makeText(this@MainActivity, "Yoy can't divide by zero", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun removeLastSymbol(str: String): String {
        return str.substring(0, str.length - 1)
    }
}